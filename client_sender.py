import json
import ssl

import websocket


def on_message(ws, message):
    print('recievied', message)


def on_error(ws, error):
    print('error', error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    print('opened')
    # def run(*args):
    #     i = 0
    #     while True:
    #         time.sleep(3)
    #         # print float(datetime.now().strftime('%s'))
    #         msg = {
    #             'status': 5,
    #             'list_users': [],
    #             'list_groups': [],
    #             'list_poss': [],
    #             'list_messages': [
    #                 {
    #                     'text': u"Мессаж нумер %d" % i,
    #                     'date': float(datetime.now().strftime('%s')),
    #                     'important': False,
    #                     'to_users': [],
    #                     'to_groups': [],
    #                     'to_poss': []
    #                 }
    #             ],
    #             'last_date': 0,
    #             'error': ''
    #         }
    #         ws.send(json.dumps(msg))
    #         i += 1
    #         # ws.close()
    #         # print("thread terminating...")
    # thread.start_new_thread(run, ())


def on_open_error(ws):
    print('open_error')


if __name__ == "__main__":
    websocket.enableTrace(True)
    # host, port = 'localhost', ':4546'
    host, port = '178.62.249.118', ':4546'
    cookie = 'session=1111; device_token=0001'
    _ws = websocket.WebSocketApp("ws://%s%s/websocket" % (host, port),
                                 on_message=on_message,
                                 on_error=on_error,
                                 on_close=on_close,
                                 on_open=on_open,
                                 cookie=cookie)
    _ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})
