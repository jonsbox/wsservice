import json

from tornado.websocket import WebSocketHandler, WebSocketClosedError


online_users = set()

class SocketHandler(WebSocketHandler):
    _user = None

    # def write_message(self, message, binary=False):
    #     try:
    #         super(SocketHandler, self).write_message(message, binary)
    #     except WebSocketClosedError:
    #         pass

    def _send_onlines(self, msg, users=None, rooms=None):
        global online_users
        users = set(users or [])
        rooms = set(rooms or [])
        was_sended = set()
        for u in online_users:
            uid = u.get_user_var().id
            if u.my_rooms & rooms or uid in users:
                if uid not in was_sended:
                    u.write_message(msg)
                    was_sended.add(uid)

    def send_message(self, msg, msg_date, important=False, rooms=None, users=None):
        self._send_onlines(msg, users, rooms)

    def get_current_user(self):
        session = self.get_cookie('session')
        device_token = self.get_cookie('device_token', None)
        user = (session, device_token)
        valids = {
            ('1111', '0001'),
            ('1112', '0002'),
            ('f516ae35f600417da56c8edd925aa93c', '6544aeebae98fb9d5521205a1349497f4645bee10d442a87e9173576f6503a7d'),
        }
        self._user = user if user in valids else None
        return self._user

    def check_origin(self, origin):
        return True

    async def open(self, *args, **kwargs):
        print('opening')
        if not self.get_current_user():
            print('not auth')
            self.close()
            return
        print('opened')
        global online_users
        online_users.add(self)

    def on_close(self):
        print('closed')
        global online_users
        if self in online_users:
            online_users.remove(self)

    async def on_message(self, message):
        print('recieved', message)
        try:
            data = json.loads(message)
            user, msg = data['to'], data['msg']
            global online_users
            for u in online_users:
                if u.get_current_user()[0] == user:
                    await u.write_message(msg)
                    return
            await self.write_message('user not found')
        except ValueError:
            await self.write_message('json error')
        except KeyError as e:
            await self.write_message('%s not found' % e.args[0])
